def my_lfu_cache(max_size):
    def cache(func):
        def wrapper(*args):
            if args in fib_cache:
                fib_cache[args] = fib_cache.get(args)[0], (fib_cache.get(args)[1] + 1)
                return fib_cache.get(args)[0]
            else:
                result = func(*args)
                wrapper.calls += 1.

            if len(fib_cache) == max_size:
                wrapper.min_val_calls, value_to_remove = wrapper.calls, 0
                for key, value in fib_cache.items():
                    if value[1] < wrapper.min_val_calls:
                        wrapper.min_val_calls = value[1]
                        value_to_remove = key
                    
                print('deleted value from cache:', value_to_remove[0], end='; ')
                fib_cache.pop(value_to_remove)

            fib_cache[args] = result, 1
            print('added value to cache:', *args)
            print('Cache:', fib_cache)
            return result

        fib_cache = {}
        wrapper.calls = 0
        return wrapper
    return cache


@my_lfu_cache(7)
def fib(n):
    fibonacci = [0, 1]
    for i in range(2, n + 1):
        fibonacci.append(fibonacci[i - 1] + fibonacci[i - 2])
    return fibonacci[n]


[(fib(i)) for i in range(5)]
[(fib(i)) for i in range(7)]
[(fib(i)) for i in range(3, 10)]





